﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.Singleton
{
    public interface IWriter
    {
        void WriteDown(string message);
    }
}
