﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace Lab3_mapz.Singleton
{
    public class Logger
    {
        private static Logger onlyObject;
        public static Logger Instance
        {
            get => onlyObject ??= new Logger();
        }
        public IWriter Writer { get; set; }

        private Logger()
        {
            var defaultLogFile = Environment.CurrentDirectory + @"\myLog.txt";
            var defaultWritter = new TextFileWriter(defaultLogFile);
            Writer = defaultWritter;
        }
        public void LogEvent(string eventDescription)
        {
            string record = string.Format("{0} || {1}", DateTime.Now, eventDescription);
            Writer.WriteDown(record);
        }
    }
}
