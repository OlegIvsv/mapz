﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.Singleton
{
    public class TextFileWriter : IWriter
    {
        private string pathToFile;

        public TextFileWriter(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentException("Incorrect path to file.");
            pathToFile = path;
        }

        public void WriteDown(string message)
        {
            using (StreamWriter sw = new StreamWriter(pathToFile, append:true))
                sw.WriteLine(message);
        }
    }
}
