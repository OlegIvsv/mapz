﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AdditionalPattern
{
    internal class Manager
    {
        private ComputerBuilder builder;

        public Manager(ComputerBuilder builder)
        {
            this.builder = builder;
        }

        public ComputerBuilder Builder
        {
            set { builder = value; }
        }

        public Computer GetComputer()
        {
            builder.Create();
            builder.SetROM();
            builder.SetRAM();
            builder.SetProcesor();
            return builder.GetResult();
        }
    }
}
