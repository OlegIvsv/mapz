﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AdditionalPattern
{
    public class OptimalComputerBuilder : ComputerBuilder
    {
        public override void SetProcesor()
        {
            var processor = new Processor { Clockspeed = 2.9, NumberOfCores = 6 };
            currentComputer.Processor = processor;
        }

        public override void SetRAM()
        {
            currentComputer.RAM = 8;
        }

        public override void SetROM()
        {
            currentComputer.ROM = 256;
        }
    }
}
