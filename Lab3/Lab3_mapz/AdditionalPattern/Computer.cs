﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AdditionalPattern
{
    public class Computer
    {
        public int ROM { get; set; }
        public int RAM { get; set; }
        public Processor Processor { get; set; }
        public override string ToString()
        {
            return string.Format("The computer has : \n ROM {0} gb\n RAM {1}gb\n Processor : {2}", 
                ROM, RAM, Processor);
        }
    }
}
