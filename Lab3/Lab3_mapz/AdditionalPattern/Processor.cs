﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AdditionalPattern
{
    public class Processor
    {
        public double Clockspeed { get; init; }
        public int NumberOfCores { get; init; }
        public override string ToString()
        {
            return $"Clockspeed - {Clockspeed}GHz; Cores - {NumberOfCores}";
        }
    }
}
