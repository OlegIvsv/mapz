﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AdditionalPattern
{
    internal class ExpensiveComputerBuilder : ComputerBuilder
    {
        public override void SetProcesor()
        {
            var processor = new Processor { NumberOfCores = 12, Clockspeed = 4.1 };
            currentComputer.Processor = processor;

        }

        public override void SetRAM()
        {
            currentComputer.RAM = 16;
        }

        public override void SetROM()
        {
            currentComputer.ROM = 1024;
        }
    }
}
