﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AdditionalPattern
{
    public class CheapComputerBuilder : ComputerBuilder
    {
        public override void SetProcesor()
        {
            var processor = new Processor { Clockspeed = 1.7, NumberOfCores = 2 };
            currentComputer.Processor = processor;
        }

        public override void SetRAM()
        {
            currentComputer.RAM = 4; 
        }

        public override void SetROM()
        {
            currentComputer.ROM = 128;
        }
    }
}
