﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AdditionalPattern
{
    public abstract class ComputerBuilder
    {
        protected Computer currentComputer;

        public void Create() => currentComputer = new Computer();

        public Computer GetResult() => currentComputer;

        public abstract void SetROM();

        public abstract void SetRAM();

        public abstract void SetProcesor();
    }
}
