﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AbstractFactory.IDEs
{
    class CLion : IDE
    {
        public CLion(params string[] extensions) : base(extensions) 
        {
        }

        public override string ToString()
        {
            return $"CLion ({base.ToString()})";
        }
    }
}
