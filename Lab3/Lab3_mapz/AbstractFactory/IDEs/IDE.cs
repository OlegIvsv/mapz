﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AbstractFactory
{
    class IDE
    {
        public List<string> Extensions = new List<string>();
        
        public IDE(params string[] extension)
        {
            Extensions.AddRange(extension);
        }

        public override string ToString()
        {
            var extensionsLine = Extensions.Aggregate(string.Empty, (sum, next) => sum + next + ';');
            return $"Extensions : {(Extensions.Count == 0 ? "Empty" : extensionsLine)}";
        }
    }
}
