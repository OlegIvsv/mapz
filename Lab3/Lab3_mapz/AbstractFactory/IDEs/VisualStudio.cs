﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AbstractFactory.IDEs
{
    class VisualStudio : IDE
    {
        public VisualStudio(params string[] extensions) : base(extensions)
        {
        }

        public override string ToString()
        {
            return $"MS Visual Studio ({base.ToString()})";
        }
    }
}
