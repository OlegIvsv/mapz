﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AbstractFactory.IDEs
{
    class WEBStorm : IDE
    {
        public WEBStorm(params string[] extensions) : base(extensions)
        {
        }

        public override string ToString()
        {
            return $"WEB Storm ({base.ToString()})";
        }
    }
}
