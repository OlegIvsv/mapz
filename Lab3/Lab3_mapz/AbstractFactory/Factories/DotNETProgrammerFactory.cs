﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3_mapz.AbstractFactory.IDEs;
using Lab3_mapz.AbstractFactory.OperatngSystems;
using Lab3_mapz.AbstractFactory.Devises;


namespace Lab3_mapz.AbstractFactory
{
    internal class DotNETProgrammerFactory : IProgrammerFactory
    {
        public Device CreateDevice()
        {
            return new Laptop("ACER Laptop");
        }

        public IDE CreateIDE()
        {
            return new VisualStudio();
        }

        public OperatingSystem CreateOperatingSystem()
        {
            return new Windows("Windows 10", 20);
        }
    }
}
