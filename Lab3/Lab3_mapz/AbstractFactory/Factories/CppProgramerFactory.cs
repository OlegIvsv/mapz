﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3_mapz.AbstractFactory.IDEs;
using Lab3_mapz.AbstractFactory.OperatngSystems;
using Lab3_mapz.AbstractFactory.Devises;


namespace Lab3_mapz.AbstractFactory
{
    internal class CppProgramerFactory : IProgrammerFactory
    {
        public Device CreateDevice()
        {
            return new Computer("Computer");
        }

        public IDE CreateIDE()
        {
            return new WEBStorm();
        }

        public OperatingSystem CreateOperatingSystem()
        {
            return new Linux("Ubuntu 21.10 (Impish Indri)", 11);
        }
    }
}
