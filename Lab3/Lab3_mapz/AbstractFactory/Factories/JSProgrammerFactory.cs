﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3_mapz.AbstractFactory.IDEs;
using Lab3_mapz.AbstractFactory.OperatngSystems;
using Lab3_mapz.AbstractFactory.Devises;

namespace Lab3_mapz.AbstractFactory
{
    internal class JSProgrammerFactory : IProgrammerFactory
    {
        public Device CreateDevice()
        {
            return new MacBook("MakBook Pro");
        }

        public IDE CreateIDE()
        {
            return new WEBStorm();
        }

        public OperatingSystem CreateOperatingSystem()
        {
            return new MacOS("MacOS Monterey", 14);
        }
    }
}
