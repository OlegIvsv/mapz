﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AbstractFactory.Devises
{
    class Computer : Device
    {
        public Computer(string name) : base(name)
        {
        }
        public override string ToString()
        {
            return "Computer " + base.ToString();
        }
    }

}
