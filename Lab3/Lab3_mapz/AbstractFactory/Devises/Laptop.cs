﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AbstractFactory.Devises
{
    class Laptop : Device
    {
        public Laptop(string name) : base(name)
        {
        }
        public override string ToString()
        {
            return "Laptop " + base.ToString();
        }
    }
}
