﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AbstractFactory
{
    internal class Programmer
    {
        public Device Device { get; set; }
        public IDE IDE { get; set; }
        public OperatingSystem OperatingSystem{ get; set; }

        public Programmer(Device devise, IDE ide, OperatingSystem os)
        {
            this.Device = devise;
            this.IDE = ide;
            this.OperatingSystem = os;
        }
        public Programmer(IProgrammerFactory factory)
        {
            Device = factory.CreateDevice();
            IDE = factory.CreateIDE();
            OperatingSystem = factory.CreateOperatingSystem();
        }
        public void WriteCode()
        {
            Console.WriteLine($"Programmer are writing code with {IDE}. " +
                $"He uses his {Device} with {OperatingSystem}");
        }
    }
}
