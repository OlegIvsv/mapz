﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AbstractFactory.OperatngSystems
{
    class MacOS : OperatingSystem
    {
        public MacOS(string version, int requiredSpace) : base(version, requiredSpace)
        {
        }

        public override string ToString()
        {
            return $"Linux Operating System (version : {Version})";
        }
    }
}
