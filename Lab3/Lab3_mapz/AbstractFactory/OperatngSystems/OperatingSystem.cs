﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_mapz.AbstractFactory
{
    internal class OperatingSystem
    {
        public string Version { get; init; }
        public int RequiredSpace { get; init; }
        public OperatingSystem(string version, int requiredSpace)
        {
            Version = version; 
            RequiredSpace = requiredSpace;
        }
        public override string ToString()
        {
            return $"Operating system | version : {Version}";
        }
    }

}
