﻿using System;
using Lab3_mapz.AbstractFactory;
using Lab3_mapz.AdditionalPattern;
using Lab3_mapz.Singleton;

namespace Lab3_mapz
{
    static class Program
    {
        public static void Main()
        {
            TestPatterns();
        }

        public static void TestPatterns()
        {
            TestAbstractFactory();
            TestBuilder();
            TestSingleton();
        }

        public static void TestAbstractFactory()
        { 
            TestProgrammer("C++", new CppProgramerFactory());
            TestProgrammer(".NET", new DotNETProgrammerFactory());
            TestProgrammer("JavaScript", new JSProgrammerFactory());
        }

        public static void TestProgrammer(string type, IProgrammerFactory factory)
        {
            Console.WriteLine("Create and use a factory for {0} programmer's set : ", type);
            var programmer = new Programmer(factory);
            programmer.WriteCode();
            Console.WriteLine();
        }

        public static void TestBuilder()
        {
            TestSpecificBuilder("cheap", new CheapComputerBuilder());
            TestSpecificBuilder("optimal", new OptimalComputerBuilder());
            TestSpecificBuilder("expensive", new ExpensiveComputerBuilder());
        }

        public static void TestSpecificBuilder(string type, ComputerBuilder builder)
        {
            Console.WriteLine("Create and use builder for {0} computer : ", type);
            var mannager = new Manager(builder);
            var computer = mannager.GetComputer();
            Console.WriteLine(computer);
        } 

        public static void TestSingleton()
        {
            var logger = Logger.Instance;
            logger.LogEvent("Test!!!");
        }
    }
}