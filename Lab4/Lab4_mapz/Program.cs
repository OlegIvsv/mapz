﻿using System;
using Lab4_mapz.Facade;
using Lab4_mapz.Composer;
using Lab4_mapz.Decorator;
using System.IO;
using System.Diagnostics;

namespace Lab4_mapz {
    static class Program
    {
        public static void Main()
        {
            GoDevices();
        }

        public static void GoFileTreePrinter()
        {
            (new FileSystemPrinter(new FileTreePresenter())).PrintSystemFor(@"D:\Desktop\testDir");
        }

        public static void GoGame()
        {
            var game = new GameFacade();
            while (!game.IsOver)
                game.MakeStep();
            Console.WriteLine("Game Is Over !");
        }

        public static void GoDevices()
        {
            IDevice device = new Computer("ACER Aspire 5", 21000, "Intel Core i5", 8);
            PrintDevice(device);
            Console.WriteLine();

            device = new AntivirusDeviceDecorator(device);
            PrintDevice(device);
            Console.WriteLine();

            device = new WarrantyDeviceDecorator(device);
            PrintDevice(device);
            Console.WriteLine();
        }

        public static void PrintDevice(IDevice device)
        {
            Console.WriteLine(device.GetModel());
            Console.WriteLine($"Price : {device.GetPrice()}");
            Console.WriteLine($"Parameters : {device.GetParameters()}");
        }
    }
}
