﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Decorator
{
    internal abstract class DeviceDecorator : IDevice
    {
        protected IDevice baseDevice;

        public DeviceDecorator(IDevice device)
        {
            baseDevice = device;
        }

        public abstract string GetModel();

        public abstract string GetParameters();

        public abstract int GetPrice();
    }
}
