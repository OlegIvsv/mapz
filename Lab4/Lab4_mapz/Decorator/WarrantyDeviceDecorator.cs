﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Decorator
{
    internal class WarrantyDeviceDecorator : DeviceDecorator
    {
        const int WaranteePrice = 120;

        public WarrantyDeviceDecorator(IDevice device) : base(device)
        {
        }

        public override string GetModel()
        {
            return baseDevice.GetModel() + " | Waranty included";
        }

        public override string GetParameters()
        {
            return baseDevice.GetParameters() + " Waranty;";
        }

        public override int GetPrice()
        {
            return baseDevice.GetPrice() + WaranteePrice;
        }
    }
}
