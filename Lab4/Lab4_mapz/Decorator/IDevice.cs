﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Decorator
{
    internal interface IDevice
    {
        abstract public string GetModel();

        abstract public int GetPrice();

        public abstract string GetParameters();
    }
}
