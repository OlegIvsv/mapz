﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Decorator
{
    internal class Computer : IDevice
    {
        private int price;
        private string model;
        private string processorModel;
        private int rom;

        public Computer(string model, int price, string processor, int rom)
        {
            this.processorModel = processor;
            this.rom = rom;
            this.price = price;
            this.model = model;
        }

        public string GetModel() => model;

        public string GetParameters()
        {
            return $"Processor : {processorModel}; ROM : {rom}gb;";
        }

        public int GetPrice() => price;
    }
}
