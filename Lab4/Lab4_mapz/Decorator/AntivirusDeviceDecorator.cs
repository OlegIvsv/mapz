﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Decorator
{
    internal class AntivirusDeviceDecorator : DeviceDecorator
    {
        const int AntivirusPrice = 100;

        public AntivirusDeviceDecorator(IDevice device) : base(device)
        {
        }

        public override string GetModel()
        {
            return baseDevice.GetModel() + " | Antivirus Included";
        }

        public override string GetParameters()
        {
            return baseDevice.GetParameters() + " With Antivirus;";
        }

        public override int GetPrice()
        {
            return baseDevice.GetPrice() + AntivirusPrice;
        }
    }
}
