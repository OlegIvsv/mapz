﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Composer
{
    internal class Node
    {
        public Node(string name, int level)
        {
            Name = name;
            Level = level;
        }
        public string Name { get; set; }
        public int Level { get; set; }
    }

    class FileNode : Node
    {
        public FileNode(string name, int level) : base(name, level)
        {
        }
    }

    class DirNode : Node, IEnumerable<Node>
    {
        private List<Node> Nodes = new List<Node>();

        public void Add(Node newNode)
            => Nodes.Add(newNode);

        public DirNode(string name, int level) : base(name, level)
        {
        }

        public IEnumerator<Node> GetEnumerator()
        {
            return Nodes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
