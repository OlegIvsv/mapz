﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab4_mapz.Composer
{
    internal static class FileTreeBuilder
    {  
        public static DirNode Build(string path)
        {
            string nameOfDir = new DirectoryInfo(path).Name;
            var mainNode = new DirNode(nameOfDir, 0);
            Build(path, mainNode);
            return mainNode;
        }

        private static void Build(string startDirPath, DirNode startDir)
        {
            var dir = new DirectoryInfo(startDirPath);
            foreach (var file in dir.GetFiles())
            {
                startDir.Add(new FileNode(file.Name, startDir.Level + 1));
            }
            foreach (var directory in dir.GetDirectories())
            {
                var newNode = new DirNode(directory.Name, startDir.Level + 1);
                startDir.Add(newNode);
                Build(directory.FullName, newNode);
            }
        }
    }
}
