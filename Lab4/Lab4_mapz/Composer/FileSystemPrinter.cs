﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Composer
{
    internal class FileSystemPrinter
    {
        private FileTreePresenter presenter;
        public FileSystemPrinter(FileTreePresenter presenter)
        {
            this.presenter = presenter;
        }
        public void PrintSystemFor(string pathToDirectory)
        {
            var tree = FileTreeBuilder.Build(pathToDirectory);
            presenter.PresentTree(tree);
        }
    }
}
