﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Composer
{
    internal class FileTreePresenter
    {
        public char SpaceChar { get; set; } = '-';
        public int LengthOfStep { get; set; } = 5;
        public ConsoleColor FileColor { get; set; } = ConsoleColor.Yellow;
        public ConsoleColor DirColor { get; set; } = ConsoleColor.Blue;
        public void PresentTree(DirNode startNode)
        {
            foreach (var node in startNode)
            {
                if (node is FileNode)
                {
                    Console.Write(new string(SpaceChar, LengthOfStep * node.Level));
                    Console.ForegroundColor = FileColor;
                    Console.WriteLine(node.Name);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.Write(new string(SpaceChar, LengthOfStep * node.Level));
                    Console.ForegroundColor = DirColor;
                    Console.WriteLine(node.Name);
                    Console.ForegroundColor = ConsoleColor.White;
                    PresentTree(node as DirNode);
                }
            }
        }
    }
}
