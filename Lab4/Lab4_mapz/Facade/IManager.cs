﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Facade
{
    internal interface IManager<T>
    {
        IList<T> GetCombination();
        bool CheckCombination(IList<T> combination);
    }
}
