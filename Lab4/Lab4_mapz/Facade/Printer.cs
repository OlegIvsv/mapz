﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Lab4_mapz.Facade
{
    internal class Printer : IPrinter
    {
        public void PrintCombination<T>(IList<T> combination)
        {
            string combinationLine = "Combination : " + 
                combination.Aggregate(string.Empty, (sum, next) => sum + next.ToString() + "  ");
            LetThink(combination.Count, combinationLine);
        }

        private void LetThink(int seconds, string combinationLine)
        {
            while (seconds > 0)
            {
                Console.Clear();
                Console.WriteLine(combinationLine);
                Console.WriteLine(seconds + "s");
                Thread.Sleep(1000);
                --seconds;
            }
            Console.Clear();
        }
    }
}
