﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Facade
{
    internal class LevelsManager : IManager<int>
    {
        private int level = 2;
        private const int MaxValue = 10;
        private int[] lastCombination = default;

        public IList<int> GetCombination() 
        {
            ResetLevelToNext();

            var combination = new int[level];
            var rand = new Random();
            for (int i = 0; i < combination.Length; i++)
                combination[i] = rand.Next(MaxValue);

            lastCombination = combination;
            return combination.ToArray();
        }

        private void ResetLevelToNext()
        {
            ++level;
        }

        public bool CheckCombination(IList<int> combination)
        {
            var numbers = combination.ToArray();

            if (numbers.Length != lastCombination.Length)
                return false;
            for (int i = 0; i < lastCombination.Length; i++)
                if (lastCombination[i] != numbers[i])
                    return false;

            return true;
        }
    }
}
