﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Facade
{
    internal class Reader : IReader<int>
    {
        public IList<int> ReadCombination()
        {
            string input = Console.ReadLine();
            var parts = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            try
            {
                return ParseNumbers(parts);
            }
            catch
            {
                return Array.Empty<int>();
            }
        }

        private int[] ParseNumbers(string[] parts)
        {
            return parts.Select(x => Convert.ToInt32(uint.Parse(x))).ToArray();
        }
    }
}
