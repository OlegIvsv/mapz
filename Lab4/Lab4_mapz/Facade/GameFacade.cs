﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_mapz.Facade
{
    internal class GameFacade
    {
        public bool IsOver { get; private set; }
        private IPrinter printer;
        private IManager<int> levelManager;
        private IReader<int> reader;

        public GameFacade(IManager<int> m, IPrinter p, IReader<int> r)
        {
            printer = p;
            levelManager = m;
            reader = r;
        }

        public GameFacade() : this(new LevelsManager(), new Printer(), new Reader())
        {
        }

        public void MakeStep()
        {
            printer.PrintCombination(levelManager.GetCombination());
            IsOver = !levelManager.CheckCombination(reader.ReadCombination());
        }
    }
}
