﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1.InheritanceOfStruct
{
    internal interface IForm
    {
        double GetSquare();
        double GetPerimeter();
    }
}
