﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1.InheritanceOfStruct
{
    internal struct Triangle : IForm
    {
        public double A { get; set; } = 0;
        public double B { get; set; } = 0;
        public double C { get; set; } = 0;

        public Triangle()
        {
        }

        public double GetPerimeter()
        {
            return A + B + C;
        }

        public double GetSquare()
        {
            return 0.5 * A * Math.Sqrt(Math.Pow(0.5 * A, 2) + Math.Pow(B, 2));
        }
    }
}
