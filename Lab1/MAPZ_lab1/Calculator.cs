﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1
{
    internal static class Calculator
    {
        public static void Sum(out int result, params int[] elems)
        {
            result =  elems.Sum();
        }
        public static void Multiplication(ref int result, params int[] elems)
        {
            if(elems.Length != 0)
                result = elems.Aggregate(1, (m, x) => x * m);
        }
    }
}
