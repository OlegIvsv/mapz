﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1.Inheritance
{
    internal interface IVehicle
    {
        void Move();
        double GetMaxSpeed();
        double GetSpeed();
        bool IsMoving();
    }
}
