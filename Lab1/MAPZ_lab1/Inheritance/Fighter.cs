﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1.Inheritance
{
    public class Fighter : Aircraft
    {
        public Fighter(string name, string producer) : base(name, producer)
        {
        }
        public Fighter() : base()
        {
        }
        public override double GetMaxSpeed()
        {
            return 800;
        }
        protected override void PrintMessageAboutStart()
        {
            Console.WriteLine("The fighter is starting moving!!!");
        }
        public override double GetMaxLoad()
        {
            return 1200;
        }
        public double GetSpeedPerCent()
        {
            return 100 * (base.CurrentSpeed / this.GetMaxSpeed());
        }

        protected new object MemberwiseClone()
        {
            return new Fighter(this.Name, this.Producer);
        }
    }
}
