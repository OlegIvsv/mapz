﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1.Inheritance
{
    internal interface ITransporter
    {
        double GetMaxLoad();
        TransportationMethod GetTransportationMethod();
    }
}
