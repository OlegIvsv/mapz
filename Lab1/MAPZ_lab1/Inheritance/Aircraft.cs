﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1.Inheritance
{
    public abstract class Aircraft : IVehicle, ITransporter
    {
        private static int CountOfExisting { get;  set; }

        protected double CurrentSpeed = default;
        public string Name { get; init; }
        public string Producer { get; init; }

        public Aircraft(string name, string producer)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(producer))
                throw new ArgumentException("Incorrent data");
            this.Name = name;
            this.Producer = producer;

            ++CountOfExisting;
        }
        public Aircraft() : this("Unknown name", "Unknown producer")
        {

        }
        static Aircraft()
        {
            CountOfExisting = 0;
        }

        public double GetSpeed()
        {
            return CurrentSpeed;
        }
        public bool IsMoving()
        {
            return CurrentSpeed == 0;
        }
        public void Move()
        {
            if (IsMoving())
                return;
            PrintMessageAboutStart();
            PrintInfo();
        }
        public void PrintInfo()
        {
            Console.WriteLine($"Name : Aircraft");
            Console.WriteLine($"Producer : Unknown");
        }
        public void PrintInfo(string format)
        {
            Console.WriteLine(format, Name, Producer);
        }

        protected virtual void PrintMessageAboutStart()
        {
            Console.WriteLine("The aircraft is starting moving!!!");
        }
        public TransportationMethod GetTransportationMethod()
        {
            return TransportationMethod.Air;
        } 
        public abstract double GetMaxSpeed();
        public abstract double GetMaxLoad();


        public override string ToString()
        {
            return $"Aircraft {Name}. Producer - {Producer}";
        }
        public override bool Equals(object? obj)
        {
            return obj is Aircraft aircraft &&
                   Name == aircraft.Name &&
                   Producer == aircraft.Producer;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Producer);
        }
        ~Aircraft()
        {
            --CountOfExisting;
        }
    }
}
