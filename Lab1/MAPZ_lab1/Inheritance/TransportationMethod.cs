﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1.Inheritance
{
    public enum TransportationMethod
    {
        Ground = 1,
        Water = 2,
        Air = 3
    }
}
