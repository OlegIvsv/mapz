﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1
{
    internal class Point
    {
        public int X;
        public int Y;
        public int Z;

        public double GetLength() =>  Math.Sqrt(X * X + Y * Y + Z * Z);

        public static implicit operator double(Point point)
        {
            return point.GetLength();
        }

        public static explicit operator string(Point point)
        {
            return $"({point.X}; {point.Y}; {point.Z})";
        }
    }
}
