﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab1
{
    public class Outter
    {
        public int OutterData { get; set; }
        private Inner InstanceOfInner { get; set; }

        public Outter(int outterData, double innerData)
        {
            InstanceOfInner = new Inner(innerData);
        }

        private class Inner
        {
            public double InnerData { get; set; } 
            public Inner(double data)
            {
                InnerData = data;
            }
        }
    }
}
