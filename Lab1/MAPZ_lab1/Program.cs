﻿using System;
using MAPZ_lab1.Inheritance;

namespace MAPZ_lab1
{
    public class Program
    {
        public static void Main()
        {
            Point point = new Point {X = 2, Y = 5, Z = 1 };
            double pointInDouble = point;
            string pointInString = (string)point;

            Console.WriteLine(pointInDouble + "\n" + pointInString);
        }

        public static void UseBoxingUnboxing<T>(T element) where T : struct
        {
            object boxed = element; //boxing
                                    //and
            T unboxed = (T)element; //unboxing
        }
    }
}