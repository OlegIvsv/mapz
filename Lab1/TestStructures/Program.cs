﻿using System;
using System.Diagnostics;

class Program
{
    public static void Main()
    {
        int numberOfExperiments = 20000000;
        IResultsPrinter printer = new ConsoleResultsPrinter();
        PointTester tester = new PointTester();

        printer.PrintResults(numberOfExperiments, "Number of experiments");
        printer.PrintResults(tester.TestCreatingDestruction(numberOfExperiments), "Time of creation and destruction");
        printer.PrintResults(numberOfExperiments, "Number of experiments");
        printer.PrintResults(tester.TestSetting(numberOfExperiments), "Time of creation and destruction");

        Console.Read();
    }
}
public interface IResultsPrinter
{
    void PrintResults<T>(T value, string name);
}
public class ConsoleResultsPrinter : IResultsPrinter
{
    public void PrintResults<T>(T value, string name)
    {
        Console.WriteLine($"{name} : {value}");
    }
}
public class PointTester
{
    protected Stopwatch stopwatch = new Stopwatch();

    public PointTester()
    {
        stopwatch = new Stopwatch();
    }
    public double TestCreatingDestruction(int numberOfExperiments)
    {
        stopwatch.Reset();
        stopwatch.Start();
        for (int i = 0; i < numberOfExperiments; i++)
        {
            using (Point point = new(1, 2, 3))
            {
            }
        }
        stopwatch.Stop();

        return stopwatch.ElapsedMilliseconds;
    }
    public double TestSetting(int numberOfExperiments)
    {
        stopwatch.Reset();
        stopwatch.Start();
        Point p = new(1, 2, 3);
        for (int i = 0; i < numberOfExperiments; i++)
        {
            p.X = i;
            p.Y = i;
            p.Z = i;
        }
        stopwatch.Stop();

        return stopwatch.ElapsedMilliseconds;
    }
}
public struct Point : IDisposable
{
    public int X;
    public int Y;
    public int Z;

    public Point(int x, int y, int z)
    {
        X = x;
        Y = y;
        Z = z;
    }
    public void Dispose() 
    {
    }
}