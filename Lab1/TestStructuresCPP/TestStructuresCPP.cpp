﻿#include <iostream>
#include <ctime>

const int NUMBER_OF_EXPERIMENTS = 20000000;

class Point{

public:
    int X;
    int Y;
    int Z;

    Point(int x, int y, int z){
        X = x;
        Y = y;
        Z = z;
    }

    ~Point() {
    }

};

int main(){

    unsigned start = clock();
    for (size_t i = 0; i < NUMBER_OF_EXPERIMENTS; i++){
        Point point = Point(1, 2, 3);
    }
    unsigned finish = clock();
    unsigned time = finish - start;

    std::cout << "Number of experiments : " << NUMBER_OF_EXPERIMENTS << "\n";
    std::cout << "Time of creation and destruction: " << time << "ms\n";


    Point point = Point(1,2,3);
    start = clock();
    for (size_t i = 0; i < NUMBER_OF_EXPERIMENTS; i++) {
        point.X = i;
        point.Y = i;
        point.Z = i;
    }
    finish = clock();
    time = finish - start;

    std::cout << "Number of experiments : " << NUMBER_OF_EXPERIMENTS << "\n";
    std::cout << "Time of setting: " << time << "ms\n";

    std::cin.get();
}