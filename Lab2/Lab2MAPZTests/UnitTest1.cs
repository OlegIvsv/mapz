using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab2MAPZ;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Lab2MAPZTests
{
    [TestClass]
    public class UnitTest1
    {
        public Phone[] PhonesData { get; set; } = Array.Empty<Phone>();

        [TestInitialize]
        public void InitializeTest()
        {
            PhonesData = new Phone[]
            {
                new("Redmi Note 10 Pro", "XIAOMI", 9999, 6, 64, 6.4),
                new("Galaxy S21 FE", "SAMSUNG", 23999, 8, 256, 6.33),
                new("Galaxy S20 FE", "SAMSUNG", 12500, 6, 128, 6.2),
                new("iPhone 11", "APPLE", 24555, 4, 128, 5.9),
                new("Galaxy A32", "SAMSUNG", 6799, 4, 64, 6.5),
                new("iPhone 13 Pro Max", "APPLE", 52000, 8, 256, 6.1),
                new("SM-G991B", "SAMSUNG", 31999, 8, 64, 6.7),
                new("iPhone 13 Mini", "APPLE", 13000, 3, 128, 5.4),
                new("Redmi Note 10 Pro", "VIVO", 9799, 6, 64, 6.4),
                new("V21", "VIVO", 18999, 8, 256, 6.1),
                new("Readmi 9 Pro", "XIAOMI", 9299, 6, 64, 6.2),
                new("Redmi Note 10", "XIAOMI", 6999, 4, 128, 6.25),
                new("Nord AC2003", "ONEPLUS", 17999, 16, 512 , 6.33)
            };
        }
        //1. �������� ������� ���������� (����� Select)
        [TestMethod]
        public void TestSelectPrices()
        {
            //Arrange
            var expected = new int[] { 9999, 23999, 12500, 24555, 6799};
            var inputData = PhonesData[..5];

            //Act
            var result = inputData.Select(p => p.Price).ToArray();

            //Assert
            CollectionAssert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSelectProducersUniqe()
        {
            //Arrange
            var expected = new string[] { "ONEPLUS", "XIAOMI", "APPLE", "SAMSUNG", "VIVO" };
            var inputData = PhonesData;

            //Act
            var result = inputData.Select(p => p.Producer)
                .Distinct()
                .ToArray();

            //Assert
            CollectionAssert.AreEqual(expected.OrderBy(s => s).ToArray(), 
                                      result.OrderBy(s => s).ToArray());
        }

        //2. ������ ����� ���������� (Where)
        [TestMethod]
        public void TestWhereProducer()
        {
            //Arrange
            var expected = new Phone[] { PhonesData[0], PhonesData[10], PhonesData[11] };
            string producerName = "XIAOMI";
            var inputData = PhonesData;

            //Act
            var result = inputData.Where(p => p.Producer == producerName).ToArray();

            //Assert
            CollectionAssert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestWherePriceHigherThanAverage()
        {
            //Arrange
            var inputData = PhonesData;

            var expected = new List<Phone>();
            double avetagePrice = inputData.Select(p => p.Price).Average();
            foreach (var phone in PhonesData)
                if(phone.Price > avetagePrice)
                    expected.Add(phone);

            //Act
            var result = (from p in inputData
                          where p.Price > PhonesData.Average(x => x.Price)
                          select p)
                         .ToArray();

            //Assert
            CollectionAssert.AreEqual(expected, result);
        }

        //3.�������� �� � ������� List ��� � � ��������� Dictionary
        [TestMethod]
        public void TestAddingAndRemovingWithList()
        {
            //Arrange
            Phone newPhone = new("A-45", "Nokia", 23000, 4, 128, 5.8);
            var data = PhonesData[..4].ToList();
            var expected = new List<Phone>() { PhonesData[1], PhonesData[2], newPhone };

            //Act
            data.RemoveAt(3);
            data.RemoveAt(0);
            data.Add(newPhone);

            //Assert
            CollectionAssert.AreEqual(expected, data);
        }

        [TestMethod]
        public void TestAddingAndRemovingWithDictionary()
        {
            //Arrange
            Phone newPhone = new("A-45", "Nokia", 23000, 4, 128, 5.8);
            var data = PhonesData[..4].ToDictionary(p => p.Model);
            var expected = new Dictionary<string, Phone>() 
            {
                { PhonesData[1].Model, PhonesData[1] },
                { PhonesData[2].Model, PhonesData[2] },
                { newPhone.Model, newPhone}
            };

            //Act
            data.Remove("iPhone 11");
            data.Remove("Redmi Note 10 Pro");
            data["A-45"] = newPhone;

            //Assert
            CollectionAssert.AreEqual(expected.OrderBy(el => el.Key).ToList(), 
                                      data.OrderBy(el => el.Key).ToList());
        }

        //4. ���������� ������ ������ ������������. 
        //5. �������� ������������ ��������� ����� �� �������������
        [TestMethod]
        public void TestExtensions()
        {
            //Arrange
            var inputData = PhonesData;
            var expected = new
            {
                AveragePrice = inputData.Select(p => p.Price).Average(),
                MostExpensivePhone = inputData[5],
                MostCheapPhone = inputData[4]
            };

            //Act
            var result = new
            {
                AveragePrice = inputData.AveragePrice(),
                MostExpensivePhone = inputData.MostExpensive(),
                MostCheapPhone = inputData.MostCheap()
            };

            //Assert
            Assert.AreEqual(expected, result);
        }

        //6. ³���������� �� ������� ������� �������������� ������ IComparer.
        [TestMethod]
        public void TestSortingWithComparerInstance()
        {
            //Arrange
            var expected = PhonesData.OrderBy(p => p.Name).ToArray();
            var data = PhonesData;

            //Act
            Array.Sort(data, new PhonesComparerByAlphabet());

            //Assert
            CollectionAssert.AreEqual(expected, data);
        }

        //7. ������������ ������ � �����.
        [TestMethod]
        public void TestListToArray()
        {
            //Arrange
            var expected = new List<Phone>(PhonesData);
            var inputData = PhonesData;

            //Act
            var result = inputData.ToList();

            //Assert
            Assert.IsTrue(result is List<Phone>);
            CollectionAssert.AreEqual(expected, result); 
        }

        //8. ³���������� �����/������ �� ���� �� �� ������� ��������.
        [TestMethod]
        public void TestOrderByRAMAndPrice()
        {
            //Arrange
            var expected = PhonesData.ToArray();
            Array.Sort(expected, new PhonesComparerByRAMDesByPriceDes());
            var inputData = PhonesData;

            //Act
            var result = inputData.OrderByDescending(p => p.RAM)
                .ThenByDescending(p => p.Price)
                .ToArray();

            //Assert
            CollectionAssert.AreEqual(expected, result);
        }

    }
}