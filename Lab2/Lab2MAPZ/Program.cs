﻿using System;

namespace Lab2MAPZ
{
    class Program
    {
        private static void Main()
        {
            var PhonesData = new Phone[]
           {
                new("Redmi Note 10 Pro", "XIAOMI", 9999, 6, 64, 6.4),
                new("Galaxy S21 FE", "SAMSUNG", 23999, 8, 256, 6.33),
                new("Galaxy S20 FE", "SAMSUNG", 12500, 6, 128, 6.2),
                new("iPhone 11", "APPLE", 24555, 4, 128, 5.9),
                new("Galaxy A32", "SAMSUNG", 6799, 4, 64, 6.5),
                new("iPhone 13 Pro Max", "APPLE", 52000, 8, 256, 6.1),
                new("SM-G991B", "SAMSUNG", 31999, 8, 64, 6.7),
                new("iPhone 13 Mini", "APPLE", 13000, 3, 128, 5.4),
                new("Redmi Note 10 Pro", "VIVO", 9799, 6, 64, 6.4),
                new("V21", "VIVO", 18999, 8, 256, 6.1),
                new("Readmi 9 Pro", "XIAOMI", 9299, 6, 64, 6.2),
                new("Redmi Note 10", "XIAOMI", 6999, 4, 128, 6.25),
                new("Nord AC2003", "ONEPLUS", 17999, 16, 512 , 6.33)
           };

            var roms = PhonesData.Select(x => x.ROM).ToList();
            foreach (var item in roms)
            {
                Console.WriteLine(item);
            }


        }
    }

}