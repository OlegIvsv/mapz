﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2MAPZ
{
    public class Phone
    {
        public string Name { get => $"{Producer} {Model}"; }
        public string Model { get; init; }
        public string Producer { get; init; }
        public int Price { get; protected set; }
        public int RAM { get; init; }
        public int ROM { get; init; }
        public double ScreenSize { get; init; }

        public Phone(string model, string producer, int price, int ram, int rom, double screenSize)
        {
            Model = model;
            Producer = producer;
            Price = price;
            RAM = ram;
            ROM = rom;
            ScreenSize = screenSize;
        }

        public override bool Equals(object? obj)
        {
            return obj is Phone phone &&
                   Name == phone.Name &&
                   Model == phone.Model &&
                   Producer == phone.Producer &&
                   RAM == phone.RAM &&
                   ROM == phone.ROM &&
                   ScreenSize == phone.ScreenSize;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Model, Producer, RAM, ROM, ScreenSize);
        }

        public override string ToString()
        {
            return string.Format("Phone : {0}; Price : ${1} RAM : {2}, ROM {3}; Screen size: {4}",
                this.Name,this.Price, this.RAM, this.ROM, this.ScreenSize);
        }
    }
}
