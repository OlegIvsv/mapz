﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2MAPZ
{
    public class StandartPhonesComparer : IComparer<Phone>
    {
        public int Compare(Phone? x, Phone? y)
        {
           return (x?.Price ?? -1).CompareTo(y?.Price ?? -1);
        }
    }
    public class PhonesComparerByAlphabet : IComparer<Phone>
    {
        public int Compare(Phone? x, Phone? y)
        {
            return (x?.Name ?? string.Empty).CompareTo(y?.Name ?? string.Empty);
        }
    }
    public class PhonesComparerByRAMDesByPriceDes : IComparer<Phone>
    {
        public int Compare(Phone? x, Phone? y)
        {
            int r = x.RAM.CompareTo(y.RAM);
            if (r == 0)
                return x.Price.CompareTo(y.Price) * -1;
            return r * -1;
        }
    }
}
