﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2MAPZ
{
    public static class Extensions
    {
        public static double AveragePrice(this IEnumerable<Phone> instance)
        {
            return instance.Average(p => p.Price);
        }

        public static IEnumerable<string> MostPopularProducer(this IEnumerable<Phone> instance)
        {
            Dictionary<string, int> counters = new();
            foreach (var item in instance)
                if(counters.ContainsKey(item.Producer))
                    ++counters[item.Producer];
                else counters[item.Producer] = 1;

            int maxCount = counters.Max(c => c.Value);
            return counters.Where(c => c.Value == maxCount).Select(c => c.Key);
        }

        public static Phone MostExpensive(this IEnumerable<Phone> instance)
        {
            var res = instance.MaxBy(p => p.Price);
            if (res == null)
                throw new ArgumentException("All the elements were null!");
            return res;
        }


        public static Phone MostCheap(this IEnumerable<Phone> instance)
        {
            var res = instance.MinBy(p => p.Price);
            if (res == null)
                throw new ArgumentException("All the elements were null!");
            return res;
        }
    }
}
