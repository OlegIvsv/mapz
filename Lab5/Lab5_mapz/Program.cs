﻿using System;
using Lab5_mapz.TemplateMethod;
using Lab5_mapz.Strategy;
using Lab5_mapz.Observer;
using System.Collections;
using Lab5_mapz.Lab;

namespace Lab5_mapz 
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Work[] works =
            {
                new LabWork("MAPZ","Lab5", 5, Complexity.Low),
                new TermPaper("PVI", "Term work", 100, Complexity.Extrime),
                new ControlWork("PVI", "KR Google", 20, Complexity.Middle)
            };

            Logger logger = new Logger();
            Player player = new Player();

            player.AddListener(logger);
            foreach (var work in works)
                player.AddWork(work);

            Console.ReadLine();
        }

        public static void TestObserver()
        {
            var shop = new Shop();
            var customers = new Customer[]
            {
                new("Oleh Olehovich", "oleh1234@gmail.com"),
                new("Ivan Ivanenko", "ivan9999@gmail.com"),
                new("Stanislav Stanislavenko", "stanislav@gmail.com")
            };

            foreach (var customer in customers)
                shop.AddNewsListener(customer);

            shop.AddProduct(new Product("MacBook Pro", 65000, "New model of 2022."));
            shop.AddProduct(new Product("Samsung S21", 28999, "The best model of this year."));
        }

        public static void TestStrategy()
        {
            const string testFile = @"D:\Desktop\testDir\testData";
            var consoleWriter = new ConsoleWriter();
            var textWriter = new TextFileWriter(testFile + ".txt");
            var binWriter = new BinFileWriter(testFile + ".bin");

            string testData = DateTime.Now.ToString();

            var saver = new DataSaver(textWriter);
            saver.SaveData(testData);

            saver.Writer = binWriter;
            saver.SaveData(testData);

            saver = new DataSaver(consoleWriter);
            saver.SaveData(testData);
        }

        public static void TestTemplateMethod()
        {
            var car = new Car("Tesla X", 285);
            car.MakeTrip();
            Console.WriteLine();
            var plane = new Plane("Су-25", 1400);
            plane.MakeTrip();
        }
    }
}
