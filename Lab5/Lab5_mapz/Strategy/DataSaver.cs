﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Strategy
{
    internal class DataSaver
    {
        public IWriter Writer { get; set; }

        public DataSaver(IWriter writer)
        {
            Writer = writer;
        }

        public void SaveData(string data)
        {
            Writer.Write(data);
        }
    }
}
