﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Strategy
{
    internal class TextFileWriter : FileWriter
    {
        public TextFileWriter(string path) : base(path)
        {
            FileInfo fileInfo = new FileInfo(path);
            if (fileInfo.Extension != ".txt")
                throw new ArgumentException();
        }

        public override void Write(string text)
        {
            using (StreamWriter writer = new StreamWriter(fileFullName, append:true))
            {
                writer.WriteLine(text);
            }
        }
    }
}
