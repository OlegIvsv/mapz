﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Strategy
{
    internal class BinFileWriter : FileWriter
    {
        public BinFileWriter(string path) : base(path)
        {
            FileInfo fileInfo = new FileInfo(path);
            if (fileInfo.Extension != ".bin")
                throw new ArgumentException();
        }
        public override void Write(string text)
        {
            using (BinaryWriter writer = new(File.Open(fileFullName, FileMode.OpenOrCreate)))
            {
                writer.Seek(0, SeekOrigin.End);
                writer.Write(text);
            }
        }
    }
}
