﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Strategy
{
    internal abstract class FileWriter : IWriter
    {
        protected string fileFullName;
        public FileWriter(string path)
        {
            fileFullName = path;
        }
        public abstract void Write(string text);
    }
}
