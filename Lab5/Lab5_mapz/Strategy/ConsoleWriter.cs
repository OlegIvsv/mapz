﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Strategy
{
    internal class ConsoleWriter : IWriter
    {
        public ConsoleColor TextColor = ConsoleColor.Cyan;
        public void Write(string text)
        {
            var prevColor = Console.ForegroundColor;
            Console.ForegroundColor = TextColor;
            Console.WriteLine(text);
            Console.ForegroundColor = prevColor;
        }
    }
}
