﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.TemplateMethod
{
    internal class Plane : Vehicle
    {
        public const int OptimalAltitude = 10000;

        public Plane(string name, int maxSpeed) : base(name, maxSpeed)
        {
        }

        protected override void Move()
        {
            Console.WriteLine($"Plane flies at an altitude of {OptimalAltitude}kms ...");
        }

        protected override void Start()
        {
            Console.Write($"{Name} takes off. ");
            double part = MaxSpeed / 5.0;
            for (double i = 0; i <= MaxSpeed; i += part)
            {
                Console.Write($"{Math.Round(i)}km/h" + (i + part <= MaxSpeed ? " ..." : string.Empty));
            }
            Console.WriteLine();
        }

        protected override void Stop()
        {
            Console.WriteLine($"{Name} lands.");
        }
    }
}
