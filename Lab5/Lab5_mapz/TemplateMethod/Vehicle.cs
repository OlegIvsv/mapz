﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.TemplateMethod
{
    abstract class Vehicle
    {
        public string Name { get; set; }
        public int MaxSpeed { get; set; }

        public Vehicle(string name, int maxSpeed)
        {
            Name = name;
            MaxSpeed = maxSpeed;
        }

        public void MakeTrip()
        {
            Start();
            Move();
            Stop();
        }

        protected abstract void Start();

        protected abstract void Move();

        protected abstract void Stop();
    }
}
