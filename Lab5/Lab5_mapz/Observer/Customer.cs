﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Observer
{
    internal class Customer : INewsListener
    {
        public string FullName { get; init; }
        public string Email { get; set; }

        public Customer(string name, string email)
        {
            FullName = name;
            Email = email;
        }

        public void Update(string news)
        {
            Console.WriteLine($"{FullName} get message about new product at {Email}:\n\"{news}\"");
            Console.WriteLine();
        }
    }
}
