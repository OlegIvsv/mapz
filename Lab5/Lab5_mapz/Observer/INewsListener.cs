﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Observer
{
    internal interface INewsListener
    {
        public void Update(string news);
    }
}
