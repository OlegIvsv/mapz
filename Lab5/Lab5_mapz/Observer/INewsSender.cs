﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Observer
{
    internal interface INewsSender
    {
        public abstract void Send(string message);
        public abstract void AddNewsListener(INewsListener listener);
        public abstract void RemoveNewsListener(INewsListener listener);
    }
}
