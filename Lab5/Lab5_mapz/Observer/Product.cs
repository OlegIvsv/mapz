﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Observer
{
    internal class Product
    {
        public string Name { get; init; }
        public double Price { get; private set; }
        public string Description { get; init; }
        public Product(string name, double price, string description)
        {
            Name = name;
            Price = price;
            Description = description;
        }
        public override string ToString()
        {
            return $"{Name} | {Math.Round(Price, 2)}$ | Description : {Description}";
        }

    }
}
