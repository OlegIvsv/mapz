﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Observer
{
    internal class Shop : INewsSender
    {
        private List<INewsListener> listeners = new();
        private List<Product> products = new();

        public void AddNewsListener(INewsListener listener)
        {
            listeners.Add(listener);
        }

        public void RemoveNewsListener(INewsListener listener)
        {
            listeners.Remove(listener);
        }

        public void AddProduct(Product newProduct)
        {
            products.Add(newProduct);
            Send(newProduct.ToString());
        }

        public void Send(string message)
        {
            string news = $"New product available. {message}";
            foreach (INewsListener listener in listeners)
                listener.Update(news);
        }
    }
}
