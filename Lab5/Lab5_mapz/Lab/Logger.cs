﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Lab
{
    internal class Logger : IStudentListener
    {
        public void Update(string news)
        {
            Console.WriteLine(news);
            Console.WriteLine();
        }
    }
}
