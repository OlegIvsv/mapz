﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Lab
{
    class TermPaper : Work
    {
        const int TheoryTimeCofficient = 2;
        const int WritingTimeCofficient = 25;
        const int ReportTimeCofficient = 20;
        const int JustifyingTimeCofficient = 15;

        public TermPaper(string subject, string name, double points, Complexity complexity) : base(subject, name, points, complexity)
        {
        }

        public override string Justify()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * JustifyingTimeCofficient);
            Thread.Sleep(ms);
            return "The term work justified!";
        }

        public override string LearnTheory()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * TheoryTimeCofficient);
            Thread.Sleep(ms);
            return "The term paper has justified!";
        }

        public override string MakeReport()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * ReportTimeCofficient);
            Thread.Sleep(ms);
            return "The report has been created!";
        }

        public override string Write()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * WritingTimeCofficient);
            Thread.Sleep(ms);
            return $"The term work \"{this.Name}\" has been written!";
        }
    }
}
