﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Lab
{
    class LabWork : Work
    {
        const int TheoryTimeCofficient = 10;
        const int WritingTimeCofficient = 10;
        const int ReportTimeCofficient = 7;
        const int JustifyingTimeCofficient = 7;

        public LabWork(string subject, string name, double points, Complexity complexity) : base(subject, name, points, complexity)
        {
        }

        public override string Justify()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * JustifyingTimeCofficient);
            Thread.Sleep(ms);
            return "The lab has been justified!";
        }

        public override string LearnTheory()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * TheoryTimeCofficient);
            Thread.Sleep(ms);
            return "The materials has been read. Ready.";
        }

        public override string MakeReport()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * ReportTimeCofficient);
            Thread.Sleep(ms);
            return "The report has been created!";
        }

        public override string Write()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * WritingTimeCofficient);
            Thread.Sleep(ms);
            return $"The lab \"{this.Name}\" has been written!";
        }
    }
}
