﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Lab
{
    class ControlWork : Work
    {
        const int TheoryTimeCofficient = 5;
        const int WritingTimeCofficient = 15;
        const int ReportTimeCofficient = 2;
        const int JustifyingTimeCofficient = 10;

        public ControlWork(string subject, string name, double points, Complexity complexity) : base(subject, name, points, complexity)
        {
        }

        public override string Justify()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * JustifyingTimeCofficient);
            Thread.Sleep(ms);
            return "The control work has been justified!";
        }

        public override string LearnTheory()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * TheoryTimeCofficient);
            Thread.Sleep(ms);
            return "Student has prepared.";

        }

        public override string MakeReport()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * ReportTimeCofficient);
            Thread.Sleep(ms);
            return "Report has been created!";
        }

        public override string Write()
        {
            int ms = Convert.ToInt32(1000 * GetComplexityCoefficient() * WritingTimeCofficient);
            Thread.Sleep(ms);
            return "Written!";
        }
    }
}
