﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Lab
{
    internal class Player : IStudent
    {
        public int Level { get; protected set; } = 2;
        public string Name { get; protected set; } = "Oleh";

        protected List<IStudentListener> studentListeners { get; set; } = new();
        protected ConcurrentQueue<Work> activeWorks { get; set; } = new();

        private bool working = false;

        public void AddWork(Work newWork)
        {
            activeWorks.Enqueue(newWork);
            Send($"New work available! {newWork}");
            if(!working)
                DoWorksAsync();
        }

        protected void DoWorks()
        {
            while (activeWorks.Count > 0)
            {
                if (activeWorks.TryDequeue(out var work))
                {
                    string log = work.Do();
                    Send($"Finished! {log}");
                }
            }
        }

        protected async void DoWorksAsync()
        {
            working = true;
            await Task.Factory.StartNew(DoWorks);
            working = false;
        }

        public void Send(string message)
        {
            foreach (var item in studentListeners)
                item.Update(message);
        }

        public void AddListener(IStudentListener listener)
           => studentListeners.Add(listener);

        public void RemoveListener(IStudentListener listener)
           => studentListeners.Remove(listener);
    }
}
