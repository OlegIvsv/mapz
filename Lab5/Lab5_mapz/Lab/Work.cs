﻿#nullable disable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Lab
{
    abstract class Work
    {
        public string Subject { get; init; }
        public string Name { get; init; }
        public double Points { get; init; }
        public Complexity Complexity { get; init; }

        public Work(string subject, string name, double points, Complexity complexity)
        {
            Subject = subject;
            Name = name;
            Points = points;
            Complexity = complexity;
        }

        public abstract string LearnTheory();
        public abstract string Write();
        public abstract string MakeReport();
        public abstract string Justify();
        public string Do()
        {
            StringBuilder sb = new StringBuilder($"{Name} : \n");
            sb.AppendLine(LearnTheory());
            sb.AppendLine(Write());
            sb.AppendLine(MakeReport());
            sb.AppendLine(Justify());
            return sb.ToString();
        }

        public override string ToString()
        {
            return $"{Name} ({Subject}); Complexity : {Complexity}; Points : {Points};";
        }
        protected double GetComplexityCoefficient() => Complexity switch
        {
            Complexity.Low => 0.15,
            Complexity.Middle => 0.35,
            Complexity.High => 0.55,
            Complexity.Extrime => 0.7,
            Complexity.Diakonuk => 1.4,
            _ => throw new Exception("Incorrect value of complexity")
        };
    }
}