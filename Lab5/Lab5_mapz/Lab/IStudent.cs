﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_mapz.Lab
{
    interface IStudent
    {
        public abstract void Send(string message);
        public abstract void AddListener(IStudentListener listener);
        public abstract void RemoveListener(IStudentListener listener);
    }
}
